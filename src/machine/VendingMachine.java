package machine;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */
public class VendingMachine {

	boolean machinePowerStatus;
	BigDecimal insertedTotal;
	List<Coin> availableChange;
	List<Item> availableItems;
	HashMap<String, Long> itemCounts;
	HashMap<String, Long> changeCount;

	public enum Coin {
		TEN_PENCE (new BigDecimal("0.10")),
		TWENTY_PENCE (new BigDecimal("0.20")),
		FIFTY_PENCE (new BigDecimal("0.50")),
		ONE_POUND (new BigDecimal("1.00"));

		private final BigDecimal value;

		Coin(BigDecimal value) {
			this.value = value;
		}
	}

	public enum Item {
		A (new BigDecimal("0.60")),
		B (new BigDecimal("1.00")),
		C (new BigDecimal("1.70"));

		private final BigDecimal value;

		Item(BigDecimal value) {
			this.value = value;
		}
	}

	public VendingMachine() {
		this.machinePowerStatus = false;
		this.insertedTotal = new BigDecimal("0");
		this.availableChange = new ArrayList<>();
		this.availableItems = new ArrayList<>(
				Arrays.asList(Item.A, Item.B, Item.C)
		);
		this.itemCounts = assessItemCount();
		this.changeCount = assessChangeCount();
	}

	public boolean isOn() {
		return this.machinePowerStatus;
	}
	
	public void setOn() {
		this.machinePowerStatus = true;
	}
	
	public void setOff() {
		this.machinePowerStatus = false;
	}

	public BigDecimal insertedTotal() {
		return this.insertedTotal;
	}

	public void addToInsertedTotalAndAvailableChange(Coin coin) {
		this.availableChange.add(coin);
		this.insertedTotal = this.insertedTotal.add(coin.value);
		this.changeCount = assessChangeCount();
	}

	private HashMap<String, Long> assessChangeCount() {
		return new HashMap<>(
				this.availableChange.stream()
						.collect(Collectors.groupingBy(Coin::name, Collectors.counting()))
		);
	}

	private HashMap<String, Long> assessItemCount() {
		return new HashMap<>(
				this.availableItems.stream()
						.collect(Collectors.groupingBy(Item::name, Collectors.counting()))
		);
	}

	public List<Coin> returnAppropriateChange(Item item) {
		BigDecimal difference = insertedTotal.subtract(item.value);
		BigDecimal totalChange = new BigDecimal("0");
		ArrayList<Coin> usedCoins = new ArrayList();
		for (Coin coin : this.availableChange) {
			if (coin.value.compareTo(difference) < 0) {
				totalChange = totalChange.add(coin.value);
				usedCoins.add(coin);
			}
		}
		this.availableChange.removeAll(usedCoins);
		return usedCoins;
	}

	public List<Coin> returnInsertedChange() {
		return this.availableChange;
	}

	public Item selectItem(Item selectedItem) {
		if (this.availableItems.contains(selectedItem)) {
			if (this.insertedTotal.compareTo(selectedItem.value) >= 0) {
				this.insertedTotal = this.insertedTotal.subtract(selectedItem.value);
				this.availableItems.remove(this.availableItems.stream()
						.filter(item -> item.equals(selectedItem))
						.findFirst()
						.get());
				this.itemCounts = assessItemCount();
				return selectedItem;
			}
		}
		return null;
	}

	public Integer returnSpecificChangeCount(Coin coin) {
		return this.changeCount.get(coin.name()).intValue();
	}

	public HashMap<String, Long> returnItemCount() {
		return this.itemCounts;
	}

	public HashMap<String, Long> returnChangeCount() {
		return this.changeCount;
	}
}

