package machine;

import machine.VendingMachine.Coin;
import machine.VendingMachine.Item;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {

	private static VendingMachine machine;

	@BeforeAll
	static void setUp() {
		machine = new VendingMachine();
	}

	private void addCoins(VendingMachine machine) {
		machine.addToInsertedTotalAndAvailableChange(Coin.TEN_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.FIFTY_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
	}

	// machinePowerStatus

	@Test
	public void defaultStateIsOff() {
	    VendingMachine freshMachine = new VendingMachine();
		assertFalse(freshMachine.isOn());
	}
	
	@Test
	public void turnsOn() {
		machine.setOn();
		assertTrue(machine.isOn());		
	}

	@Test
	public void turnsOff() {
		machine.setOn();
		machine.setOff();
		assertFalse(machine.isOn());
	}

	// coins

	@Test
	public void tracksInsertedTotal() {
	    VendingMachine freshMachine = new VendingMachine();
		List<Coin> coins = new ArrayList<>(
				Arrays.asList(Coin.TEN_PENCE, Coin.TWENTY_PENCE, Coin.FIFTY_PENCE, Coin.ONE_POUND)
		);

		coins.forEach(freshMachine::addToInsertedTotalAndAvailableChange);

		assertEquals(freshMachine.insertedTotal(), new BigDecimal("1.80"));
	}

	@Test
	public void returnsInsertedCoins() {
		machine.addToInsertedTotalAndAvailableChange(Coin.TEN_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		List<Coin> result = machine.returnInsertedChange();
		assertFalse(result.isEmpty());
	}

	// items

	@Test
	public void paysForItemA() {
		machine.addToInsertedTotalAndAvailableChange(Coin.FIFTY_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.TEN_PENCE);
		Item itemA = machine.selectItem(Item.A);
		assertNotNull(itemA);
	}

	@Test
	public void paysForItemB() {
		machine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		Item itemB = machine.selectItem(Item.B);
		assertNotNull(itemB);
	}

	@Test
	public void paysForItemC() {
		machine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		machine.addToInsertedTotalAndAvailableChange(Coin.FIFTY_PENCE);
		machine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		Item itemC = machine.selectItem(Item.C);
		assertNotNull(itemC);
	}

	@Test
	public void failsDueToInsufficientChange() {
		machine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		Item itemA = machine.selectItem(Item.A);
		assertNull(itemA);
	}

	@Test
	public void returnsCorrectCountOfItems() {
		assertEquals(3, machine.returnItemCount().size());
	}

	// change

	@Test
	public void selectItemAndReturnsCorrectChange() {
	    VendingMachine freshMachine = new VendingMachine();
	    addCoins(freshMachine);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		Item itemC = freshMachine.selectItem(Item.C);
		List<Coin> coins = freshMachine.returnAppropriateChange(itemC);
		assertNotNull(itemC);
		assertEquals(2, coins.size());
	}

	@Test
	public void returnsCorrectCountOfChange() {
		VendingMachine freshMachine = new VendingMachine();
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.ONE_POUND);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.FIFTY_PENCE);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.TEN_PENCE);
		assertEquals(4, freshMachine.returnChangeCount().size());
		assertEquals(2, freshMachine.returnChangeCount().get("ONE_POUND"));
	}

	@Test
	public void returnCorrectCountSpecificChange() {
	    VendingMachine freshMachine = new VendingMachine();
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		freshMachine.addToInsertedTotalAndAvailableChange(Coin.TWENTY_PENCE);
		assertEquals(3, freshMachine.returnSpecificChangeCount(Coin.TWENTY_PENCE));
	}
}
